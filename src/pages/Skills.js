// Technologies/Skills or tools, certificates, and testimonials
import { useState, useEffect } from "react";
import { Container, Row } from "react-bootstrap";
import SkillCard from "../components/SkillCard";
import skillData from "../data/skillData";

export default function Skills()
{
  const [skills, setSkills] = useState();

  useEffect(()=>
  {
    setSkills(skillData.map(skill =>
      {
        return(<SkillCard key={skill.title} skill={skill} />)
      }))
  },[])

  return(
  <>
    <Container className="text-center p-3 mt-5 mb-5">
      <h1>Skills</h1>
    </Container>
    <Container>
      <Row className="d-flex justify-content-center align-items-center text-center flex-wrap p-2">
      {skills}
      </Row>
    </Container>
    
  </>
      
  )
}