// Projects with js search and filter functions
// Each project should have: thumbnail, title, date created, description, technologies, notes
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import ProjectCard from "../components/ProjectCard";
import projectData from "../data/projectData";

export default function Projects()
{
  const [projects, setProjects] = useState();

  useEffect(()=>
  {
    setProjects(projectData.map(project =>
      {
        return(<ProjectCard key={project.id} project={project} />)
      }))
  },[])

  return(
  <>
    <Container className="text-center p-3 mt-5 mb-5">
      <h1>Projects</h1>
    </Container>
    {projects}
  </>
      
  )
}