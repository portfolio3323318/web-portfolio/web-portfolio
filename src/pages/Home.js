// Landing, about me, and contacts
import { useState } from "react";
import { Card, Container, OverlayTrigger, Tooltip, Row, Col } from "react-bootstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import UP from "../images/UP.png";
import Zuitt from "../images/Zuitt.png";


export default function Home()
{
  const [copied, setCopied] = useState(false);
  const email = 'tesorocelina@gmail.com';

  const copyToClipboard = () => 
  {
    navigator.clipboard.writeText(email);
    setCopied(true);
  };

  const mailTooltip = (
    <Tooltip id='mail-tooltip'>
    {copied ? 'Email copied to clipboard' : 'Copy email to clipboard'}
  </Tooltip>

  );

  return(
    <>
    <Container className="text-center p-3 mb-2">
      <h1>Celina Tesoro</h1>
      <h3>Full Stack Web Developer</h3>

      <div>
      <a href='https://www.linkedin.com/in/celina-tesoro/' target='_blank' rel='noopener noreferrer'>
        <FontAwesomeIcon icon={faLinkedin} size='3x' className='m-3' />
      </a>

      <OverlayTrigger placement='bottom' overlay={mailTooltip}>
        <FontAwesomeIcon icon={faEnvelope} size='3x' className='m-3' onClick={copyToClipboard} style={{cursor: 'pointer'}} />
      </OverlayTrigger>
      </div>
    </Container>

    <Container className="text-center p-3">
      <p>I am an engineer who found greater passion in web development. While I am new to the industry, I enjoy programming both with purpose and for fun.</p>
    </Container>

    <Container className="text-center p-3 mt-5 mb-5">
      <h1>Education</h1>
    </Container>

  <Container className="overflow-auto">
    <Card className="p-3 justify-content-center  mb-5 bg-light border-0">
      <Row className="d-flex justify-content-center align-items-center text-center flex-wrap p-2">
        <Col md={4} className="p-0 mb-4 mb-md-0 order-md-1">
          <Card.Img variant="top" src={Zuitt} className="card-img p-3" style={{ width: '300px', height: '300px' }} />
        </Col>
        <Col md={8} className="order-md-2">
          <Card.Title className="mb-4">Zuitt</Card.Title>
          <Card.Subtitle className="m-2">Best in Full Stack Development</Card.Subtitle>
          <Card.Subtitle className="mb-4">March 2023 - April 2023</Card.Subtitle>
          <Card.Text style={{ textAlign: 'justify' }}>I completed a two-month, fast-paced coding bootcamp, regarded as the leading program for web development in the Philippines. With a focus on MERN Stack, I excelled in debugging, consistently ranking among the top performers in my class. My aptitude for learning quickly continues to drive my development as a web developer. </Card.Text>
        </Col>
      </Row>
    </Card>
  </Container>

  <Container className="overflow-auto">
    <Card className="p-3 justify-content-center  mb-5 bg-light border-0">
      <Row className="d-flex justify-content-center align-items-center text-center flex-wrap p-2">
        <Col md={4} className="p-0 mb-4 mb-md-0 order-md-1">
          <Card.Img variant="top" src={UP} className="card-img p-3" style={{ width: '300px', height: '300px' }} />
        </Col>
        <Col md={8} className="order-md-2">
          <Card.Title className="mb-4">University of the Philippines - Diliman</Card.Title>
          <Card.Subtitle className="m-2">BS Civil Engineering (cum laude)</Card.Subtitle>
          <Card.Subtitle className="mb-4">2013 - 2018</Card.Subtitle>
          <Card.Text style={{ textAlign: 'justify' }}>My undergraduate thesis involved developing a traffic simulation using specialized software. Our curriculum also included three programming classes using C. My general weighted average for these classes is 1.25. I took the Civil Engineering Licensure Exam the same year I graduated and passed on my first attempt. </Card.Text>
        </Col>
      </Row>
    </Card>
  </Container>

    </>
    
  )
}