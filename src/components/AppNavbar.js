// Navbar for navigating between pages. Mobile responsive.

import {useState, useEffect} from "react";
import { Navbar, Nav } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faBriefcase, faCode } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom';

function AppNavbar() 
{
  const [isMobile, setIsMobile] = useState(false);
 
	// making different navbars render depending on screensize

	const SmallNavbar = () =>
	{
		return(
		  <Navbar bg="light" expand="md" className="p-3 mt-10 justify-content-center" fixed="bottom">
		    <Nav className="d-flex align-items-center flex-row">
		      <Nav.Link as={NavLink} to="/" className="p-2 text-center" active={window.location.pathname === '/'}>
		        <div className="d-flex flex-column">
		          <FontAwesomeIcon icon={faHome} size="lg" />
		          <div>Home</div>
		        </div>
		      </Nav.Link>

          <Nav.Link as={NavLink} to="/projects" className="p-2 text-center" active={window.location.pathname === '/projects'}>
		        <div className="d-flex flex-column">
		          <FontAwesomeIcon icon={faBriefcase} size="lg" />
		          <div>Projects</div>
		        </div>
		      </Nav.Link>

          <Nav.Link as={NavLink} to="/skills" className="p-2 text-center" active={window.location.pathname === '/skills'}>
		        <div className="d-flex flex-column">
		          <FontAwesomeIcon icon={faCode} size="lg" />
		          <div>Skills</div>
		        </div>
		      </Nav.Link>
		    </Nav>
		  </Navbar>
		)

	}

	const LargeNavbar = () =>
	{
		return (
		  <Navbar bg="light" expand="md" className="p-3" fixed="top">
		    <Navbar.Brand as={NavLink} to="/"active={window.location.pathname === '/'}>
		      <span className="ms-2">Celina Tesoro</span>
		    </Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" className="mr-auto">
		      <Nav className="ms-auto">
		        <Nav.Link as={NavLink} to="/" className="p-2" active={window.location.pathname === '/'}>
		          <FontAwesomeIcon icon={faHome} size="lg" />
		          <span className="ms-2">Home</span>
		        </Nav.Link>

            <Nav.Link as={NavLink} to="/projects" className="p-2" active={window.location.pathname === '/projects'}>
		          <FontAwesomeIcon icon={faBriefcase} size="lg" />
		          <span className="ms-2">Projects</span>
		        </Nav.Link>

            <Nav.Link as={NavLink} to="/skills" className="p-2" active={window.location.pathname === '/skills'}>
		          <FontAwesomeIcon icon={faCode} size="lg" />
		          <span className="ms-2">Skills</span>
		        </Nav.Link>
		      </Nav>
		    </Navbar.Collapse>
		  </Navbar>
		);

	}

	// const [nav, setNav] = useState(SmallNavbar);
	// toggling visibility of each navbar between screen sizes

	/*{window.innerWidth < 576 ? <SmallNavbar /> : <LargeNavbar />}*/

	useEffect(() => {
	  const handleResize = () => {
	    setIsMobile(window.innerWidth < 768); // set isMobile based on screen width
	  };
	  handleResize(); // initial call to set isMobile on component mount
	  window.addEventListener('resize', handleResize); // add event listener for resize
	  return () => {
	    window.removeEventListener('resize', handleResize); // remove event listener on component unmount
	  };
	}, []);

	return(
		<>
    {isMobile ?
    <SmallNavbar />
    :
    <LargeNavbar />
  	}
    </>
	)

  /*return (
    <Navbar bg="light" expand="lg" sticky="top" className="d-lg-flex justify-content-between">
      <Navbar.Toggle aria-controls="navbar-nav" />
      <Navbar.Brand>Celina Tesoro</Navbar.Brand>
      <Navbar.Collapse id="navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link as={Link} to="/" exact active={location.pathname === '/'}>
            <FontAwesomeIcon icon={faHome} className="mr-lg-2" />
            <span>Home</span>
          </Nav.Link>
          <Nav.Link as={Link} to="/projects" active={location.pathname === '/projects'}>
            <FontAwesomeIcon icon={faBriefcase} className="mr-lg-2" />
            <span>Projects</span>
          </Nav.Link>
          <Nav.Link as={Link} to="/skills" active={location.pathname === '/skills'}>
            <FontAwesomeIcon icon={faCode} className="mr-lg-2" />
            <span>Skills</span>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );*/
}

export default AppNavbar;
