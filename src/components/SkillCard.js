import { Card, Col } from 'react-bootstrap';

const SkillCard = ({ skill }) => 
{
  const {title, image} = skill;
  return (
  <Col xs={12} md={3}>
    <Card className="h-100 text-center m-3 p-3 bg-light">
      <div className='text-center'>
      <Card.Img variant="top" src={image} style={{ width: '100px', height: '100px', objectFit: 'contain' }} />
      </div>
      <Card.Body>
        <Card.Title>{title}</Card.Title>
      </Card.Body>
    </Card>
  </Col>
  );
};

export default SkillCard;
