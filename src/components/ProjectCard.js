// Projects with js search and filter functions
// Each project should have: thumbnail, title, date created, description, technologies, notes
import { Card, Container, Row, Col } from "react-bootstrap"

export default function ProjectCard({project})
{
    const {/*id,*/ thumbnail, title, link, date, technologies, description} = project;

    return(
    <>  
      <Container className="overflow-auto">
        <Card className="p-3 justify-content-center  mb-5 bg-light border-0">
          <Row className="d-flex justify-content-center align-items-center text-center flex-wrap p-2">
            <Col md={4} className="p-0 mb-4 mb-md-0 order-md-1">
              <Card.Img variant="top" src={thumbnail} className="card-img p-3" style={{ width: '300px', height: '300px' }} />
            </Col>
            <Col md={8} className="order-md-2">
              <Card.Title className="mb-4"><a href={link} target='_blank' rel="noreferrer" className ="stretched-link" style={{color: 'inherit','text-decoration': 'none'}}>{title}</a></Card.Title>
              <Card.Subtitle className="m-2">{technologies.join(", ")}</Card.Subtitle>
              <Card.Subtitle className="mb-4">{date.toLocaleDateString('en-US', { year: 'numeric', month: 'long', day: 'numeric' })}</Card.Subtitle>
              <Card.Text style={{ textAlign: 'justify' }}>{description} </Card.Text>
            </Col>
          </Row>
        </Card>
      </Container>
    </>
        
    )
}