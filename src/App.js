import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap'

import './App.css';
import AppNavbar from './components/AppNavbar';

import Home from './pages/Home';
import Projects from './pages/Projects';
import Skills from './pages/Skills';



function App() 
{
  return (
    <>
    <Router>
      <AppNavbar/>
      <Container>
        <Routes>     
            <Route path="/" element={<Home/>}/>
            <Route path="/projects" element={<Projects/>}/>
            <Route path="/skills" element={<Skills/>}/> 
        </Routes>
      </Container>
    </Router>
    </>
  );
}

export default App;
