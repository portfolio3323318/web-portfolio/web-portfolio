import Bootstrap from '../images/logos/logo-bootstrap.png'
import ChatGPT from '../images/logos/logo-chatgpt.png'
import CSS from '../images/logos/logo-css3.png'
import Express from '../images/logos/logo-expressjs.png'
import Git from '../images/logos/logo-git.png'
import GitHub from '../images/logos/logo-github.png'
import GitLab from '../images/logos/logo-gitlab.png'
import HTML from '../images/logos/logo-html5.png'
import Javascript from '../images/logos/logo-javascript.png'
import MongoDB from '../images/logos/logo-mongodb.png'
import NodeJs from '../images/logos/logo-nodejs.png'
import Postman from '../images/logos/logo-postman.png'
import React from '../images/logos/logo-react.png'
import Render from '../images/logos/logo-render.png'
import Rest from '../images/logos/logo-rest.png'
import Sass from '../images/logos/logo-sass.png'
import Sublime from '../images/logos/logo-sublime-text-3.png'
import Vercel from '../images/logos/logo-vercel.png'
import VSCode from '../images/logos/logo-vscode.png'

const skillData =
[
    {
        title: "Bootstrap",
        image: Bootstrap
    },
    {
        title: "ChatGPT",
        image: ChatGPT
    },
    {
        title: "CSS",
        image: CSS
    },
    {
        title: "Express",
        image: Express
    },
    {
        title: "Git",
        image: Git
    },
    {
        title: "GitHub",
        image: GitHub
    },
    {
        title: "GitLab",
        image: GitLab
    },
    {
        title: "HTML",
        image: HTML
    },
    {
        title: "Javascript",
        image: Javascript
    },
    {
        title: "MongoDB",
        image: MongoDB
    },
    {
        title: "NodeJs",
        image: NodeJs
    },
    {
        title: "Postman",
        image: Postman
    },
    {
        title: "React",
        image: React
    },
    {
        title: "Render",
        image: Render
    },
    {
        title: "Rest",
        image: Rest
    },
    {
        title: "Sass",
        image: Sass
    },
    {
        title: "Sublime",
        image: Sublime
    },
    {
        title: "Vercel",
        image: Vercel
    },
    {
        title: "VSCode",
        image: VSCode
    }
];

export default skillData;