// Projects with js search and filter functions
// Each project should have: thumbnail, title, date created, description, technologies, notes(?)
import Shawp from '../images/projects/Shawp.png'
const projectData = 
[
  {
    id: 1,
    thumbnail: Shawp,
    title: "Shawp",
    link: "https://fullstack-capstone-3-tesoro-shawp-app.vercel.app/",
    date: new Date(2023, 3, 26),
    technologies: ["MongoDB", "Express", "React", "Node", "HTML", "CSS", "Sass"],
    description: `This project served as the culminating capstone for our bootcamp, with a deadline of 2 days for the back end and 2.5 days for the full stack development. Despite the time constraints, I went above and beyond the requirements and won Best in Full Stack Development for my project. The website is fully responsive for mobile use and features a functional log-in system, with different interfaces for admin users and regular users. Notably, I self-taught Sass and successfully integrated it into the project without any guidance from my instructor. 

    Kindly note that the website is hosted on Vercel, while the back end is hosted on Render. Due to dormancy, the initial load time may be slower. Please allow a few minutes for Render to scale up server speed.`
  }
];

export default projectData;